//
//  InitVCViewController.m
//  KidShop
//
//  Created by Evgen Sichkar on 1/14/14.
//  Copyright (c) 2014 Evgen Sichkar. All rights reserved.
//

#import "InitVCViewController.h"

@interface InitVCViewController ()

@end

@implementation InitVCViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationItem setLeftBarButtonItem:nil];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
