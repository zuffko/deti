//
//  Parser.h
//  Интернет Магазин Дети
//
//  Created by Evgen Sichkar on 12/24/13.
//  Copyright (c) 2013 Evgen Sichkar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Product.h"

@interface Parser : NSObject
-(NSMutableArray*)parseCatalog:(NSData*)data;
-(NSMutableArray*)parseSubCatalog:(NSData*)data term:(NSString *)term;
-(NSMutableArray*)parseProductList:(NSData*)data;
-(Product*)parseProduct:(NSData*)data;
-(NSMutableArray*)parseCartProducts:(NSData*)data;
-(NSMutableArray*)ParseNews:(NSData*)data;
@end
