//
//  UserSettingsViewController.h
//  KidShop
//
//  Created by Evgen Sichkar on 1/30/14.
//  Copyright (c) 2014 Evgen Sichkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerAPI.h"

@interface UserSettingsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UITextField *changePass;
@property (weak, nonatomic) IBOutlet UITextField *reperatePassTF;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *fioTF;
@property (weak, nonatomic) IBOutlet UITextField *birthday;
@property (weak, nonatomic) IBOutlet UITextField *city;
@property (weak, nonatomic) IBOutlet UITextField *kidName1;
@property (weak, nonatomic) IBOutlet UITextField *kidBirthday1;
@property (weak, nonatomic) IBOutlet UITextField *kidName2;
@property (weak, nonatomic) IBOutlet UITextField *kidBirthday2;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *cancellButton;

@end
