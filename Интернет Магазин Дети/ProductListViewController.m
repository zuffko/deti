//
//  ProductListViewController.m
//  Интернет Магазин Дети
//
//  Created by Evgen Sichkar on 12/24/13.
//  Copyright (c) 2013 Evgen Sichkar. All rights reserved.
//

#import "ProductListViewController.h"
#import "ProductListCell.h"
#import "ProductListIttem.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ProductViewController.h"

@interface ProductListViewController ()

@end

@implementation ProductListViewController{
    ServerAPI *request;
    NSMutableArray *productArray; //массив храняший список товаров
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"prod list %@", [userDefaults objectForKey:@"sessionId"]);
    
    request = [ServerAPI new];
    request.delegate=self;
    //запрос списка товаров
    [request getProductListwithPath:_subCatalogId orCatalogId:nil completion:^(BOOL success, id result, NSError *error) {
        productArray = result;
        [self.collectionView reloadData];
    }];

    UIImage* image2 = [UIImage imageNamed:@"basket@2x.png"];
    CGRect frame2 = CGRectMake(0, 0, 20, 20);
    UIButton* someButton2 = [[UIButton alloc] initWithFrame:frame2];
    [someButton2 setBackgroundImage:image2 forState:UIControlStateNormal];
    [someButton2 setShowsTouchWhenHighlighted:YES];
    [someButton2 addTarget:self action:@selector(pushCart) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* someBarButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:someButton2];
    [self.navigationItem setRightBarButtonItem:someBarButtonItem2];
}

-(void)pushCart{
    CartViewController *catr = [self.storyboard instantiateViewControllerWithIdentifier:@"cart"];
    [self.navigationController pushViewController:catr animated:YES];
}

-(void)setProductArrayWithArray:(NSMutableArray*)array{
    
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [productArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductListCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    ProductListIttem *item = [productArray objectAtIndex:indexPath.row];
    cell.label.text=item.name;
    cell.price.text = [NSString stringWithFormat:@"Цена %@руб.", [[item.price componentsSeparatedByString:@","] objectAtIndex:0]];
    [cell.price setTextColor:[UIColor colorWithRed:255.0/255.0 green:110/255.0 blue:110/255.0 alpha:1]];
    [cell.image setImageWithURL:item.img placeholderImage:nil];

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductListIttem *item = [productArray objectAtIndex:indexPath.row];
    ProductViewController *productListController = [self.storyboard instantiateViewControllerWithIdentifier:@"productViewController"];
    productListController.productId = [NSString stringWithFormat:@"%@/%@", _subCatalogId,item.productId];
    productListController.navigationItem.title = item.name;
     [self.navigationController pushViewController:productListController animated:YES];
}
@end
