//
//  OrderViewController.m
//  KidShop
//
//  Created by Evgen Sichkar on 1/30/14.
//  Copyright (c) 2014 Evgen Sichkar. All rights reserved.
//

#import "OrderViewController.h"

@interface OrderViewController ()

@end

@implementation OrderViewController{
    ServerAPI *request;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    request = [ServerAPI new];
    request.delegate=self;
    _priceLabel.text = _price;

	
    
}

-(void)viewDidAppear:(BOOL)animated{
    [_nameTF becomeFirstResponder];
    [_nameTF resignFirstResponder];
}

- (IBAction)confirm:(id)sender {
    if ([_nameTF.text length]>0||[_family.text length]>0||[_emailTF.text length]>0||[_phoneTF.text length]>0||[_cityTF.text length]>0||[_commentTV.text length]>0||[_stereet.text length]>0) {
        [request counfirm];
    }else{
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Корзина"
                                                             message:@"Заполните все поля"
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
        [errorAlert show];

    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
