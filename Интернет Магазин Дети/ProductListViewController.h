//
//  ProductListViewController.h
//  Интернет Магазин Дети
//
//  Created by Evgen Sichkar on 12/24/13.
//  Copyright (c) 2013 Evgen Sichkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerAPI.h"
#import "CartViewController.h"
@interface ProductListViewController : UICollectionViewController <ServerAPI>
@property (nonatomic, retain) NSString *catalogId;
@property (nonatomic, retain) NSString *subCatalogId;
-(void)setProductArrayWithArray:(NSMutableArray*)array;
@end
