//
//  ServerAPI.h
//  Интернет Магазин Дети
//
//  Created by Evgen Sichkar on 12/24/13.
//  Copyright (c) 2013 Evgen Sichkar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Parser.h"

@protocol ServerAPI <NSObject>
@optional
@end

//блок для возвращения результатов запроса
typedef void (^ServerAPICompletion)(BOOL success, id result, NSError *error);

@interface ServerAPI : NSObject

#pragma mark - действия с каталогом и заказом товара

-(void)getCatalogСompletion:(ServerAPICompletion)completion; //получение каталога
-(void)getSubCatalogWithCatalogId:(NSString*)catalogId completion:(ServerAPICompletion)completion;//получение субкаталога
-(void)getProductListwithPath:(NSString*)subcatalogId orCatalogId:(NSString*)catalogId completion:(ServerAPICompletion)completion;//получение списка товаров
-(void)getProductWithId:(NSString*)productId completion:(ServerAPICompletion)completion; //получение информации о товаре
-(void)getNoveltytWithPage:(NSString*)page completion:(ServerAPICompletion)completion; //получение новинок (нужно подправить под новое апи)
-(void)getActionProductsWithPage:(NSString*)page completion:(ServerAPICompletion)completion; //получение акционных товаров(нужно подправить под новое апи)
-(void)getBestsellersProductsWithPage:(NSString*)page completion:(ServerAPICompletion)completion; //получение списка популярных товаров (нужно подправить под новое апи)
-(void)getBrandsWithPage:(NSString*)page completion:(ServerAPICompletion)completion; //получение списка брендов (нужно подправить под новое апи)
-(void)getNewsWithPage:(NSString*)page; //получение списка новостей
-(void)addToCartWithProductId:(NSString*)productId andSessionId:(NSString*)sessionId andAmount:(NSString*)amount; //добавление товара в корзину
-(void)GetCart; //получение списка товаров в корзине
-(void)count; //изменение количества товаров в корзине
-(void)counfirm; //подтверждение заказа (виталий должен переделать метод)

#pragma mark - Вход, информация и настройки пользователя

-(void) loginUserWithEmail:(NSString*)email andPassword:(NSString*)password complection:(ServerAPICompletion)complection;
-(void) signUpWithEmail:(NSString*)email phone:(NSString*)phone name:(NSString*)name city:(NSString*)city card:(NSString*)card completion:(ServerAPICompletion)completion;
-(void) getUserSettingWithUserId:(NSString*)userId completion:(ServerAPICompletion)completion;
-(void) saveUserSettingsWithEmail:(NSString*)email oldPass:(NSString*)oldPass newPass:(NSString*)newPass phone:(NSString*)phone name:(NSString*)name birthDate:(NSString*)birthDate city:(NSString*)city kidName:(NSString*)kidName kidDate:(NSString*)kidDate completion:(ServerAPICompletion)completion;
@property (nonatomic, assign) id <ServerAPI> delegate;
@end




