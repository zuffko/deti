//
//  ProductViewController.m
//  Интернет Магазин Дети
//
//  Created by Evgen Sichkar on 12/27/13.
//  Copyright (c) 2013 Evgen Sichkar. All rights reserved.
//

#import "ProductViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ProductViewController{
    ServerAPI *request;
    Product *loadetProduct;
}

- (void)viewDidLoad
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"product %@", [userDefaults objectForKey:@"sessionId"]);
    
    [super viewDidLoad];
    request = [ServerAPI new];
    request.delegate=self;
    [request getProductWithId:_productId completion:^(BOOL success, id result, NSError *error) {
        
    }];
    _scrollView.delegate = self;
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, 1000);
    _scrollView.scrollEnabled=YES;
    _scrollView.userInteractionEnabled=YES;
	// Do any additional setup after loading the view.
    [self.scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width, self.view.bounds.size.height)];
    [_scrollView setScrollEnabled:YES];
    [self.view addSubview:_scrollView];
    
    UIImage* image2 = [UIImage imageNamed:@"basket@2x.png"];
    CGRect frame2 = CGRectMake(0, 0, 20, 20);
    UIButton* someButton2 = [[UIButton alloc] initWithFrame:frame2];
    [someButton2 setBackgroundImage:image2 forState:UIControlStateNormal];
    [someButton2 setShowsTouchWhenHighlighted:YES];
    [someButton2 addTarget:self action:@selector(pushCart) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* someBarButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:someButton2];
    [self.navigationItem setRightBarButtonItem:someBarButtonItem2];
}

-(void)pushCart{
    CartViewController *catr = [self.storyboard instantiateViewControllerWithIdentifier:@"cart"];
    [self.navigationController pushViewController:catr animated:YES];
}

-(void)loadproduct:(Product*)product{
    loadetProduct = product;
    if (loadetProduct) {
        NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:loadetProduct.name];
        _nameProduct.attributedText = string;
        
        NSMutableAttributedString * brand = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Производитель: %@", loadetProduct.brand]];
        [brand addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(14, [brand length]-14)];
        _brand.attributedText = brand;
        
        NSMutableAttributedString * article = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Артикул товара: %@",loadetProduct.article]];
        [article addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(15, [article length]-15)];
        _article.attributedText = article;
        
        NSMutableAttributedString * price = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Цена: %@ руб.",loadetProduct.price]];
        [price addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(5, [price length]-5)];
        _price.attributedText= price;
        
        NSMutableAttributedString * socialPrice = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Цена по социальной карте: %@ руб.",loadetProduct.socialCardPrice]];
        [socialPrice addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(25, [socialPrice length]-25)];
        _socialPrice.attributedText = socialPrice;
        
        UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];
        CGSize stringSizeDescription =[loadetProduct.description sizeWithFont:font constrainedToSize:CGSizeMake(300.0f, 999.0f) lineBreakMode:NSLineBreakByWordWrapping];
        _description.text = loadetProduct.description;
        _description.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];
        CGRect frame = _description.frame;
        frame.size.height = stringSizeDescription.height;
        _description.frame=frame;
        [self.scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width, self.view.bounds.size.height+stringSizeDescription.height)];
        
        NSString *image = [loadetProduct.images objectAtIndex:0];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://dev.online.detishop.ru%@",image]];
        [_image setImageWithURL:loadetProduct.img placeholderImage:nil];
        
        NSLog(@"%@", loadetProduct.images);
        if ([loadetProduct.images count]>0) {
            NSMutableArray *buttons = [[NSMutableArray alloc] initWithObjects:_bt1, _by2, _bt3, nil];
            int i;
            for (i=0; i<[loadetProduct.images count] ; i++) {
                UIButton *dutton = buttons[i];
                UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL: [NSURL URLWithString:[NSString stringWithFormat:@"http://dev.online.detishop.ru/%@", loadetProduct.images[i]]]]];
                
                //[dutton setImage:image forState:UIControlStateNormal];
                [dutton setBackgroundImage:image forState:UIControlStateNormal];
            }
        }
    }else{
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"ошибка сервера"
                                                             message:@"Неправильный ответ сервера! Попробуйте загрузить другой товар"
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
        [errorAlert show];
    }
}

- (IBAction)addToCart:(id)sender {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *result = [userDefaults objectForKey:@"sessionId"];
    
    [request addToCartWithProductId:loadetProduct.productId andSessionId:result andAmount:@"1"];
    //[request counfirm];
}
//07a738729f22cc2e652d25d425baf2ee


@end
