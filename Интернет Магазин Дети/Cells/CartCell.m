//
//  CartCell.m
//  KidShop
//
//  Created by Evgen Sichkar on 1/9/14.
//  Copyright (c) 2014 Evgen Sichkar. All rights reserved.
//

#import "CartCell.h"

@implementation CartCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

+(CartCell*)cell{
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CartCellId" owner:nil options:nil];
    return [nibObjects objectAtIndex:0];
}

+(NSString*)cellID
{
    return @"CartCellId";
}

- (IBAction)plus:(id)sender {
    int i = [_countLabel.text intValue];
    i++;
    _countLabel.text = [NSString stringWithFormat:@"%i", i];
    
}

- (IBAction)minus:(id)sender {
    if ([_countLabel.text intValue]>1) {
        int i = [_countLabel.text intValue];
        i--;
        _countLabel.text = [NSString stringWithFormat:@"%i", i];
    }
}

@end
