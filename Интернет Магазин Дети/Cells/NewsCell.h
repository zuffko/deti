//
//  NewsCell.h
//  KidShop
//
//  Created by Evgen Sichkar on 1/9/14.
//  Copyright (c) 2014 Evgen Sichkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UITextView *text;
+(NewsCell*)cell;
+(NSString *)cellID;
@end
