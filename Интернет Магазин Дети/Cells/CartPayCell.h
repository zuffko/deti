//
//  CartPayCell.h
//  KidShop
//
//  Created by Evgen Sichkar on 1/9/14.
//  Copyright (c) 2014 Evgen Sichkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartPayCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *priceSumma;
@property (weak, nonatomic) IBOutlet UIButton *cancel;
@property (weak, nonatomic) IBOutlet UIButton *pay;

+(CartPayCell*)cell;
+(NSString *)cellID;
@end
