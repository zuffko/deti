//
//  CartCell.h
//  KidShop
//
//  Created by Evgen Sichkar on 1/9/14.
//  Copyright (c) 2014 Evgen Sichkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIButton *countPlus;
@property (weak, nonatomic) IBOutlet UIButton *countMinus;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UILabel *price;

+(CartCell*)cell;
+(NSString *)cellID;
@end
