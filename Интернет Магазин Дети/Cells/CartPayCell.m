//
//  CartPayCell.m
//  KidShop
//
//  Created by Evgen Sichkar on 1/9/14.
//  Copyright (c) 2014 Evgen Sichkar. All rights reserved.
//

#import "CartPayCell.h"

@implementation CartPayCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

+(CartPayCell*)cell{
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"CartPayCellId" owner:nil options:nil];
    return [nibObjects objectAtIndex:0];
}

+(NSString*)cellID
{
    return @"CartPayCellId";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
