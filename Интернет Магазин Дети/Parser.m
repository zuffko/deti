//
//  Parser.m
//  Интернет Магазин Дети
//
//  Created by Evgen Sichkar on 12/24/13.
//  Copyright (c) 2013 Evgen Sichkar. All rights reserved.
//

#import "Parser.h"
#import "CatalogItem.h"
#import "SubcatalogItem.h"
#import "ProductListIttem.h"
#import "Product.h"
#import "CartProduct.h"
#import "ShortNews.h"

@implementation Parser

-(NSMutableArray*)parseCatalog:(NSData*)data{
    NSDictionary* json = data;
    NSArray *responce= [json[@"categories"] allValues];
    NSMutableArray *result = [NSMutableArray new];
    for (NSDictionary *item in responce) {
        CatalogItem *cat= [CatalogItem new];
        cat.catId=item[@"alias"];
        cat.name=item[@"name"];
        [result addObject:cat];
    }
    return result;
}

-(NSMutableArray*)parseSubCatalog:(NSData*)data term:(NSString *)term{
    NSDictionary* json = data;
    NSArray *array =  [json[@"categories"] allValues];
    NSArray *result = [NSMutableArray new];
    NSMutableArray *finalArray = [NSMutableArray array];
    for (NSDictionary *dictionary in array) {
        if ([dictionary[@"alias"] isEqualToString:term]) {
            NSArray *children = dictionary[@"children"];
            if ([children count]>1) {
                result = [dictionary[@"children"] allValues];
                for (NSDictionary *item in result) {
                    SubcatalogItem *subCatalogItem= [SubcatalogItem new];
                    subCatalogItem.subcatalogId =item[@"alias"];
                    subCatalogItem.name=item[@"name"];
                    [finalArray addObject:subCatalogItem];
                }
            }
        }
    }
    return finalArray;
}

-(NSMutableArray*)parseProductList:(NSData*)data{
    NSDictionary* json = data;
    NSMutableArray *responce= json[@"content"][@"goods"];
    NSMutableArray *result = [NSMutableArray new];
    for (NSDictionary *item in responce) {
        if (![item isKindOfClass:[NSNull class]]) {
            ProductListIttem *productLostItem= [ProductListIttem new];
            productLostItem.productId =item[@"good"][@"id"];
            productLostItem.name=item[@"good"][@"name"];
            if (![item[@"images"] isKindOfClass:[NSNull class]]) {
                productLostItem.img=[NSURL URLWithString: item[@"images"][0][@"image_medium"]];
            }else{
                NSLog(@"no photos");
            }
            
            productLostItem.price=item[@"good"][@"price"];
            productLostItem.description=item[@"good"][@"short_desc"];
            [result addObject:productLostItem];
        }
    }
    return result;
}

-(Product*)parseProduct:(NSData*)data{
    NSDictionary* json = data;
    NSMutableDictionary *responce= json[@"response"][0];//[@"detail"];
    NSMutableDictionary *nextDic = responce[@"detail"];
    Product*product = [Product new];
    
    product.article = nextDic[@"first"];
    product.brand = nextDic[@"brand"][@"name"];
    product.description = nextDic[@"full"];
    product.productId = nextDic[@"first"];
    int i = [product.article intValue];
    NSString *str = [NSString stringWithFormat:@"%i", i];
    NSMutableDictionary *dic = nextDic[@"vars"][str];
    product.name= dic[@"name"];
    NSMutableArray *imagArr = dic[@"images"];
    NSMutableDictionary *imageDic = imagArr[0];
    NSString *imageUrl =imageDic[@"image_big"];
    product.img = [NSURL URLWithString:imageUrl];
    product.price= dic[@"prices"][@"price"];
    product.socialCardPrice=dic[@"prices"][@"soc_price"];
    return product;
}

-(NSMutableArray*)parseCartProducts:(NSData*)data{
    NSDictionary* json = data;
    NSMutableArray *result = [NSMutableArray new];
    if (![json isKindOfClass:[NSArray class]]) {
        NSArray *responce= json[@"response"];
        NSMutableArray *result = [NSMutableArray new];
        for (NSDictionary *item in responce) {
            CartProduct *product = [CartProduct new];
            if ([item count]<1) {
                return nil;
            }
            product.productId= item[@"good_id"];
            product.name = item[@"name"];
            product.imageUrl = [NSURL URLWithString:item[@"img"]];
            product.count = [item[@"amount"] intValue];
            int i = [item[@"price"] intValue];
            
            product.price = [NSString stringWithFormat:@"%i", i];
//            product.socPrice = item[@"prices"][@"27"];
            [result addObject:product];
        }
        return result;
    }
    return result;
}

-(NSMutableArray*)ParseNews:(NSData*)data{
    NSDictionary* json = data;
    NSArray *responce= json[@"response"][@"news"];
    NSMutableArray *result = [NSMutableArray new];
    for (NSDictionary *item in responce) {
        if (![item isKindOfClass:[NSNull class]]) {
            ShortNews *news= [ShortNews new];
            news.newsId = item[@"id"];
            news.date =item[@"date"];
            news.intro =item[@"intro"];
            news.title =item[@"title"];
            [result addObject:news];
        }
    }
    return result;
}
@end
