//
//  SubCatalogViewController.m
//  Интернет Магазин Дети
//
//  Created by Evgen Sichkar on 12/25/13.
//  Copyright (c) 2013 Evgen Sichkar. All rights reserved.
//

#import "SubCatalogViewController.h"
#import "SubcatalogItem.h"
#import "ProductListViewController.h"


@interface SubCatalogViewController ()
@property (nonatomic, retain) NSMutableArray * subCatalogArray;//массив хранящий элементы субкаталога
@end

@implementation SubCatalogViewController{
    ServerAPI *request;
}
@synthesize catalogId=_catalogId;

- (void)viewDidLoad
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"sub catalog %@", [userDefaults objectForKey:@"sessionId"]);
    [super viewDidLoad];
    request = [ServerAPI new];
    request.delegate=self;
    //запрос субкаталога по алиасу каталога
    [request getSubCatalogWithCatalogId:_catalogId completion:^(BOOL success, id result, NSError *error) {
        
    }];
    //установка правой кнопки uinavigationitem
    UIImage* image2 = [UIImage imageNamed:@"basket@2x.png"];
    CGRect frame2 = CGRectMake(0, 0, 20, 20);
    UIButton* someButton2 = [[UIButton alloc] initWithFrame:frame2];
    [someButton2 setBackgroundImage:image2 forState:UIControlStateNormal];
    [someButton2 setShowsTouchWhenHighlighted:YES];
    [someButton2 addTarget:self action:@selector(pushCart) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* someBarButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:someButton2];
    [self.navigationItem setRightBarButtonItem:someBarButtonItem2];
}

-(void)pushCart{
    CartViewController *catr = [self.storyboard instantiateViewControllerWithIdentifier:@"cart"];
    [self.navigationController pushViewController:catr animated:YES];
}

-(void)loadSubCatalog:(NSMutableArray*)subcatalog{
    //грубо говоря сеттер субкаталога
    _subCatalogArray = subcatalog;
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_subCatalogArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellSubcatalog";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    SubcatalogItem *subCatalogItem = [_subCatalogArray objectAtIndex:indexPath.row];
    cell.textLabel.text =subCatalogItem.name;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ProductListViewController *productListController = [self.storyboard instantiateViewControllerWithIdentifier:@"productList"];
    SubcatalogItem *item = [_subCatalogArray objectAtIndex:indexPath.row];
    productListController.subCatalogId = [NSString stringWithFormat:@"%@/%@",_catalogId,item.subcatalogId];
    [self.navigationController pushViewController:productListController animated:YES];
    //[self performSegueWithIdentifier:@"subcatalogToProductList" sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"subcatalogToProductList"]){
        
        ProductListViewController *destination = [segue destinationViewController];
        SubcatalogItem *item = [_subCatalogArray objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        destination.subCatalogId = item.subcatalogId;
        destination.navigationItem.title = item.name;
    }
}

@end
