//
//  ChapterViewController.m
//  KidShop
//
//  Created by Evgen Sichkar on 1/27/14.
//  Copyright (c) 2014 Evgen Sichkar. All rights reserved.
//

#import "ChapterViewController.h"

@interface ChapterViewController ()
@property (weak, nonatomic) IBOutlet UIButton *nowelty;
@property (weak, nonatomic) IBOutlet UIButton *actions;
@property (weak, nonatomic) IBOutlet UIButton *topSales;
@property (weak, nonatomic) IBOutlet UIButton *brands;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation ChapterViewController{
    ServerAPI *request;
    NSMutableArray *productArray;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    request = [ServerAPI new];
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    
    [request getActionProductsWithPage:@"1" completion:^(BOOL success, id result, NSError *error) {
        productArray = result;
        [self.collectionView reloadData];
    }];
	// Do any additional setup after loading the view.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [productArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductListCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    ProductListIttem *item = [productArray objectAtIndex:indexPath.row];
    cell.label.text=item.name;
    cell.price.text = [NSString stringWithFormat:@"Цена %@руб.", [[item.price componentsSeparatedByString:@","] objectAtIndex:0]];
    [cell.price setTextColor:[UIColor colorWithRed:255.0/255.0 green:110/255.0 blue:110/255.0 alpha:1]];
    [cell.image setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://dev.online.detishop.ru%@",item.img]] placeholderImage:nil];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductListIttem *item = [productArray objectAtIndex:indexPath.row];
    
    
    ProductViewController *productListController = [self.storyboard instantiateViewControllerWithIdentifier:@"productViewController"];
    productListController.productId = item.productId;
    productListController.navigationItem.title = item.name;
    [self.navigationController pushViewController:productListController animated:YES];
}

@end
