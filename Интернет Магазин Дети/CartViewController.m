//
//  CartViewController.m
//  KidShop
//
//  Created by Evgen Sichkar on 1/9/14.
//  Copyright (c) 2014 Evgen Sichkar. All rights reserved.
//

#import "CartViewController.h"
#import "CartPayCell.h"
#import "CartProduct.h"
#import "CartCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "OrderViewController.h"

@implementation CartViewController{
    ServerAPI *request;
    NSMutableArray *catrArray;
    NSMutableArray *catrCellArray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    request = [ServerAPI new];
    request.delegate=self;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [request GetCart];
    
    CGRect frame2 = CGRectMake(0, 0, 70, 20);
    UIButton* someButton2 = [[UIButton alloc] initWithFrame:frame2];
    [someButton2 setTitle:@"Очист." forState:UIControlStateNormal];
    [someButton2 setShowsTouchWhenHighlighted:YES];
    [someButton2 addTarget:self action:@selector(pushCart) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* someBarButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:someButton2];
    [self.navigationItem setRightBarButtonItem:someBarButtonItem2];
}

-(void)setCart:(NSMutableArray*)catr{
    if (catr==nil) {
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Корзина"
                                                             message:@"Корзина пуста"
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
        [errorAlert show];
    }else{
        catrArray = catr;
        [self createCell];
    }
}

-(void)pushCart{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:nil forKey:@"sessionId"];
    [userDefaults synchronize];
}

-(void)createCell{
        catrCellArray = [NSMutableArray new];
        for (CartProduct* product in catrArray) {
            CartCell *cell;
            cell = [self.tableView dequeueReusableCellWithIdentifier:[CartCell cellID]];
            if (!cell) {
                cell=[CartCell cell];
            }
            cell.name.lineBreakMode = NSLineBreakByWordWrapping;
            cell.name.numberOfLines = 2;
            cell.name.text=product.name;
            cell.price.text=product.price;
            cell.countLabel.text = [NSString stringWithFormat:@"%i", product.count];
            [cell.image setImageWithURL:product.imageUrl];
            [catrCellArray addObject:cell];
        }
    if ([catrArray count]>0) {
        CartPayCell *cell;
        cell = [self.tableView dequeueReusableCellWithIdentifier:[CartPayCell cellID]];
        if (!cell) {
            cell=[CartPayCell cell];
        }
        [cell.pay addTarget:self action:@selector(pay:) forControlEvents:UIControlEventTouchUpInside];
        [cell.cancel addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
        int price = 0;
        for (CartProduct* product in catrArray) {
            price+=[product.price intValue];
        }
        cell.priceSumma.text = [NSString stringWithFormat:@"%i %@", price, @"руб."];
        [catrCellArray addObject:cell];
    }
        [self.tableView reloadData];
}

//-(void)pay:(id)sender{
//    [request counfirm];  
//}

-(void)cancel:(id)sender{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:nil forKey:@"sessionId"];
    [userDefaults synchronize];
    catrCellArray = nil;
    [self.tableView reloadData];
    //[request GetCart];
}
- (IBAction)pay:(id)sender {
    OrderViewController *order = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderViewController"];
    CartPayCell *cell = [catrCellArray lastObject];
    order.price = cell.priceSumma.text;
    [self.navigationController pushViewController:order animated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (catrCellArray) {
        return [catrCellArray count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [catrCellArray objectAtIndex:indexPath.row];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100.0;
}

@end
