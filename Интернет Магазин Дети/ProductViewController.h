//
//  ProductViewController.h
//  Интернет Магазин Дети
//
//  Created by Evgen Sichkar on 12/27/13.
//  Copyright (c) 2013 Evgen Sichkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerAPI.h"
#import "Product.h"
#import "CartViewController.h"

@interface ProductViewController : UIViewController <ServerAPI, UIScrollViewDelegate>
@property (nonatomic, retain) NSString *productId;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UILabel *nameProduct;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UIButton *bt1;
@property (weak, nonatomic) IBOutlet UIButton *by2;
@property (weak, nonatomic) IBOutlet UIButton *bt3;
@property (weak, nonatomic) IBOutlet UILabel *brand;
@property (weak, nonatomic) IBOutlet UILabel *article;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *socialPrice;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;
@property (weak, nonatomic) IBOutlet UITextView *description;

-(void)loadproduct:(Product*)product;
@end
