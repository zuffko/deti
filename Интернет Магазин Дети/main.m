//
//  main.m
//  Интернет Магазин Дети
//
//  Created by Evgen Sichkar on 12/22/13.
//  Copyright (c) 2013 Evgen Sichkar. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
