//
//  Product.h
//  Интернет Магазин Дети
//
//  Created by Evgen Sichkar on 12/24/13.
//  Copyright (c) 2013 Evgen Sichkar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject
@property (nonatomic, retain) NSString *productId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSURL *img;
@property (nonatomic, retain) NSString *price;
@property (nonatomic, retain) NSString *socialCardPrice;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) NSString *article;
@property (nonatomic, retain) NSString *brand;
@property (nonatomic, retain) NSMutableArray *images;
@end
