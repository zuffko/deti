//
//  CatalogItem.h
//  Интернет Магазин Дети
//
//  Created by Evgen Sichkar on 12/24/13.
//  Copyright (c) 2013 Evgen Sichkar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CatalogItem : NSObject
@property (nonatomic, retain) NSString *catId;
@property (nonatomic, retain) NSString *name;
@property int subCatalogCount;
@end
