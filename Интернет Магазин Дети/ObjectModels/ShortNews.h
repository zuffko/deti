//
//  ShortNews.h
//  KidShop
//
//  Created by Evgen Sichkar on 1/9/14.
//  Copyright (c) 2014 Evgen Sichkar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShortNews : NSObject
@property (nonatomic, retain) NSString *newsId;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *intro;
@property (nonatomic, retain) NSString *date;
@end
