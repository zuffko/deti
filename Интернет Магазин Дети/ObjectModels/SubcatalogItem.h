//
//  SubcatalogItem.h
//  Интернет Магазин Дети
//
//  Created by Evgen Sichkar on 12/24/13.
//  Copyright (c) 2013 Evgen Sichkar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubcatalogItem : NSObject
@property (nonatomic, retain) NSString *subcatalogId;
@property (nonatomic, retain) NSString *name;
@end
