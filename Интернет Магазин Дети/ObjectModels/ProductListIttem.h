//
//  ProductListIttem.h
//  Интернет Магазин Дети
//
//  Created by Evgen Sichkar on 12/24/13.
//  Copyright (c) 2013 Evgen Sichkar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductListIttem : NSObject
@property (nonatomic, retain) NSString *productId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSURL *img;
@property (nonatomic, retain) NSString *price;
@property (nonatomic, retain) NSString *description;
@end
