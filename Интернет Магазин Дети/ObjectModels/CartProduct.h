//
//  CartProduct.h
//  KidShop
//
//  Created by Evgen Sichkar on 1/9/14.
//  Copyright (c) 2014 Evgen Sichkar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CartProduct : NSObject
@property (nonatomic, retain) NSString* productId;
@property (nonatomic, retain) NSURL * imageUrl;
@property (nonatomic, retain) NSString* name;
@property int count;
@property (nonatomic, retain) NSString* price;
@property (nonatomic, retain) NSString* socPrice;
@end
