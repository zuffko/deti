//
//  AppDelegate.m
//  Интернет Магазин Дети
//
//  Created by Evgen Sichkar on 12/22/13.
//  Copyright (c) 2013 Evgen Sichkar. All rights reserved.
//

#import "AppDelegate.h"
#import "SHSidebarController.h"
#import "ViewController.h"


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UINavigationBar appearance] setTintColor:[UIColor redColor]];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    NSMutableArray *vcs = [NSMutableArray array];
    //установка контроллеров для сайдбара
    for (int i = 0; i <= 11; i++){
        if (i==0){
            NSDictionary *view = [self addViewControllerWithStoryboardId:@"initPage" andTitle:@"Каталог"];
            [vcs addObject:view];
        } else if (i==1){
            NSDictionary *view = [self addViewControllerWithStoryboardId:@"catalog" andTitle:@"Каталог"];
            [vcs addObject:view];
        } else if (i==2){
            NSDictionary *view = [self addViewControllerWithStoryboardId:@"bonusSys" andTitle:@"Бонусная система"];
            [vcs addObject:view];
        } else if (i==3){
            NSDictionary *view = [self addViewControllerWithStoryboardId:@"magazimeRules" andTitle:@"Правила магазина"];
            [vcs addObject:view];
        } else if (i==4){
            NSDictionary *view = [self addViewControllerWithStoryboardId:@"delivery" andTitle:@"Доставка"];
            [vcs addObject:view];
        } else if (i==5){
            NSDictionary *view = [self addViewControllerWithStoryboardId:@"payment" andTitle:@"Оплата"];
            [vcs addObject:view];
        } else if (i==6){
            NSDictionary *view = [self addViewControllerWithStoryboardId:@"contacts" andTitle:@"Контакты"];
            [vcs addObject:view];
        } else if (i==7){
            NSDictionary *view = [self addViewControllerWithStoryboardId:@"profile" andTitle:@"Прoфиль"];
            [vcs addObject:view];
        } else if (i==8){
            NSDictionary *view = [self addViewControllerWithStoryboardId:@"news" andTitle:@"Новости"];
            [vcs addObject:view];
        } else if (i==9){
            NSDictionary *view = [self addViewControllerWithStoryboardId:@"book" andTitle:@"Книга жалоб и предложений"];
            [vcs addObject:view];
        } else if (i==10){
            NSDictionary *view = [self addViewControllerWithStoryboardId:@"aboutUs" andTitle:@"О нас"];
            [vcs addObject:view];
        } else if (i==11){
            NSDictionary *view = [self addViewControllerWithStoryboardId:@"distTorg" andTitle:@"Разделы"];
            [vcs addObject:view];
        }
    }
    //инициализация контроллера сайдабра
    SHSidebarController *sidebar = [[SHSidebarController alloc] initWithArrayOfVC:vcs];
    self.window.rootViewController = sidebar;
    [self.window makeKeyAndVisible];
    return YES;
}

-(NSDictionary*)addViewControllerWithStoryboardId:(NSString*)storyboardId andTitle:(NSString*)title{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    //установка изначального вью контроллера
    UIViewController *blue = [storyboard instantiateViewControllerWithIdentifier:storyboardId];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:blue];
    [nav.navigationBar setTranslucent:NO];
    nav.navigationBar.barTintColor = [UIColor whiteColor];
    UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 44, 320, 1)];
    view1.backgroundColor = [UIColor redColor];
    [nav.navigationBar addSubview:view1];
    NSDictionary *view = [NSDictionary dictionaryWithObjectsAndKeys:nav, @"vc", title, @"title", nil];
    return view;
}

@end
