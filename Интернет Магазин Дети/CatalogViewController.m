//
//  CatalogViewController.m
//  Интернет Магазин Дети
//
//  Created by Evgen Sichkar on 12/25/13.
//  Copyright (c) 2013 Evgen Sichkar. All rights reserved.
//

#import "CatalogViewController.h"
#import "CatalogItem.h"
#import "SubCatalogViewController.h"

@interface CatalogViewController ()
@property (nonatomic,retain)NSMutableArray*catalogArray; //массив хранящий в себе результат запроса
@end

@implementation CatalogViewController{
    ServerAPI *request;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"%@", [userDefaults objectForKey:@"sessionId"]);
    
    
    request = [ServerAPI new];
    request.delegate=self;
    //зарпос каталога
    [request getCatalogСompletion:^(BOOL success, id result, NSError *error) {
        _catalogArray=result;
        [self.tableView reloadData];
        self.navigationItem.title = @"Каталог";
    }];
    
    UIImage* image2 = [UIImage imageNamed:@"basket@2x.png"];
    CGRect frame2 = CGRectMake(0, 0, 20, 20);
    UIButton* someButton2 = [[UIButton alloc] initWithFrame:frame2];
    [someButton2 setBackgroundImage:image2 forState:UIControlStateNormal];
    [someButton2 setShowsTouchWhenHighlighted:YES];
    [someButton2 addTarget:self action:@selector(pushCart) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* someBarButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:someButton2];
    [self.navigationItem setRightBarButtonItem:someBarButtonItem2];
}

-(void)pushCart{
    //переход в корзину
    CartViewController *catr = [self.storyboard instantiateViewControllerWithIdentifier:@"cart"];
    [self.navigationController pushViewController:catr animated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_catalogArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    CatalogItem *item=[_catalogArray objectAtIndex:indexPath.row];
    cell.textLabel.text=item.name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"catalogToSubCatalogSegue" sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"catalogToSubCatalogSegue"]){
        SubCatalogViewController *destination = [segue destinationViewController];
        CatalogItem *item =[_catalogArray objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        destination.catalogId = item.catId;
        destination.navigationItem.title = item.name;
    }
}


@end
