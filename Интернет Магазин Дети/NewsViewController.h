//
//  NewsViewController.h
//  KidShop
//
//  Created by Evgen Sichkar on 1/9/14.
//  Copyright (c) 2014 Evgen Sichkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerAPI.h"

@interface NewsViewController : UITableViewController <ServerAPI>
-(void)setNews:(NSMutableArray*)array;
@end
