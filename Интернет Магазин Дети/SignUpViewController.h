//
//  SignUpViewController.h
//  KidShop
//
//  Created by Evgen Sichkar on 1/30/14.
//  Copyright (c) 2014 Evgen Sichkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerAPI.h"

@interface SignUpViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *cityTF;
@property (weak, nonatomic) IBOutlet UITextField *bonusTF;
@property (weak, nonatomic) IBOutlet UIButton *registerUser;
@end
