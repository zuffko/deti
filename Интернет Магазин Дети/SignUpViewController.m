//
//  SignUpViewController.m
//  KidShop
//
//  Created by Evgen Sichkar on 1/30/14.
//  Copyright (c) 2014 Evgen Sichkar. All rights reserved.
//

#import "SignUpViewController.h"
#import "ServerAPI.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController{
    ServerAPI *request;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)singUp:(id)sender {
    [request signUpWithEmail:_emailTF.text phone:_phoneTF.text name:_nameTF.text city:_cityTF.text card:_bonusTF.text completion:^(BOOL success, id result, NSError *error) {
        
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    request = [ServerAPI new];
    
	// Do any additional setup after loading the view.
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
