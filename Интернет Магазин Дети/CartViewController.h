//
//  CartViewController.h
//  KidShop
//
//  Created by Evgen Sichkar on 1/9/14.
//  Copyright (c) 2014 Evgen Sichkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerAPI.h"

@interface CartViewController : UIViewController <ServerAPI, UITableViewDataSource, UITableViewDelegate>
-(void)setCart:(NSMutableArray*)catr;
-(void)pay:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
-(void)cancel:(id)sender;
@end
