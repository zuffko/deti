//
//  ServerAPI.m
//  Интернет Магазин Дети
//
//  Created by Evgen Sichkar on 12/24/13.
//  Copyright (c) 2013 Evgen Sichkar. All rights reserved.
//

#import "ServerAPI.h"
#import "AFNetworking.h"
#import "Product.h"


@implementation ServerAPI{
    AFHTTPRequestOperationManager *manager;
    Parser *parser;
}

- (id)init
{
    self = [super init];
    if (self) {
        manager = [AFHTTPRequestOperationManager manager];
        parser = [Parser new];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-type"];
    }
    return self;
}

- (void) getCatalogСompletion:(ServerAPICompletion)completion {
    [manager POST:@"http://dev.online.detishop.ru/app/catalog" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableArray *new=[parser parseCatalog:responseObject];
        completion (YES,new,nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSData *data = operation.responseData;
        NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
        NSLog(@"%@", newStr);
    }];
}

- (void) getSubCatalogWithCatalogId:(NSString*)catalogId completion:(ServerAPICompletion)completion{
    [manager POST:@"http://dev.online.detishop.ru/app/catalog" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableArray *new= [parser parseSubCatalog:responseObject term:catalogId];
        SEL selectorq = @selector(loadSubCatalog:);
        [_delegate performSelector:selectorq withObject:new];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

-(void)getProductListwithPath:(NSString*)subcatalogId orCatalogId:(NSString*)catalogId completion:(ServerAPICompletion)completion{
    NSString *new = [NSString stringWithFormat:@"http://dev.online.detishop.ru/app/catalog/%@", subcatalogId];
    NSLog(@"%@", new);
    [manager POST:new  parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableArray *new=[parser parseProductList:responseObject];
        completion (YES,new,nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

-(void)getProductWithId:(NSString*)productId completion:(ServerAPICompletion)completion{
    NSString *new = [NSString stringWithFormat:@"http://dev.online.detishop.ru/app/catalog/%@", productId];
    [manager POST:new parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {

      Product*new=[parser parseProduct:responseObject];
      [_delegate performSelector:@selector(loadproduct:) withObject:new];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);

    }];
}

-(void)getNoveltytWithPage:(NSString*)page completion:(ServerAPICompletion)completion{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"2",@"page",
                            nil];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [manager POST:@"http://dev.online.detishop.ru/app/catalog/getNovelty" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSMutableArray *new= [parser parseSubCatalog:responseObject];
        //completion(YES,new,nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSData *data = operation.responseData;
        NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
        NSLog(@"%@", newStr);
    }];
}

-(void)getActionProductsWithPage:(NSString*)page completion:(ServerAPICompletion)completion{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"1",@"page",
                            nil];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [manager POST:@"http://dev.online.detishop.ru/app/catalog/getActionProducts" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSMutableArray *new= [parser parseSubCatalog:responseObject];
        //completion(YES,new,nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSData *data = operation.responseData;
        NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
        NSLog(@"%@", newStr);
    }];
}

-(void)getBestsellersProductsWithPage:(NSString*)page completion:(ServerAPICompletion)completion{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"1",@"page",
                            nil];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [manager POST:@"http://dev.online.detishop.ru/app/catalog/getBestsellersProducts" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSMutableArray *new= [parser parseSubCatalog:responseObject];
        //completion(YES,new,nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSData *data = operation.responseData;
        NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
        NSLog(@"%@", newStr);
    }];
}

-(void)getBrandsWithPage:(NSString*)page completion:(ServerAPICompletion)completion{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"1",@"page",
                            nil];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [manager POST:@"http://dev.online.detishop.ru/app/catalog/getBrands" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSMutableArray *new= [parser parseSubCatalog:responseObject];
        //completion(YES,new,nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSData *data = operation.responseData;
        NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
        NSLog(@"%@", newStr);
    }];
}

-(void)getNewsWithPage:(NSString*)page{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"1",@"page",
                            nil];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [manager POST:@"http://dev.online.detishop.ru/app/news" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableArray *array = [parser ParseNews:responseObject];
        [_delegate performSelector:@selector(setNews:) withObject:array];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSData *data = operation.responseData;
        NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
        NSLog(@"%@", newStr);
    }];
}


-(void)addToCartWithProductId:(NSString*)productId andSessionId:(NSString*)sessionId andAmount:(NSString*)amount{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    NSLog(@"sessionID %@", sessionId);
    manager = [AFHTTPRequestOperationManager manager];
    int i = [productId intValue];
    NSString *good = [NSString stringWithFormat:@"%i", i];
//        params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                amount,@"amount",nil];
    //[params setObject:good forKey:@"good"];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [manager POST:@"http://dev.online.detishop.ru/app/cart/push" parameters:@{@"amount": @"1",@"good": good} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
            NSString *session =responseObject[@"sessionId"];
        if ([session length]) {
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Корзина"
                                                                 message:@"Товар добавлен в корзину"
                                                                delegate:nil
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
            [errorAlert show];
            if (!sessionId) {
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:session forKey:@"sessionId"];
                [userDefaults synchronize];
            }
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSData *data = operation.responseData;
        NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
        NSLog(@"error %@", newStr);
    }];
}

-(void)GetCart{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *result = [userDefaults objectForKey:@"sessionId"];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            result,@"sessionId",
                            nil];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [manager POST:@"http://dev.online.detishop.ru/app/cart/popup" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"cart %@", responseObject);
        NSMutableArray *new=[parser parseCartProducts:responseObject];
        [_delegate performSelector:@selector(setCart:) withObject:new];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSData *data = operation.responseData;
        NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
        NSLog(@"error %@", newStr);
    }];
}

-(void)count{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"0e23db2ef47046c005daf3e0f4b0d17c",@"sessionId",
                            @"8575",@"good",
                            @"2",@"amount",
                            nil];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [manager POST:@"http://dev.online.detishop.ru/app/cart/change" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"cart %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSData *data = operation.responseData;
        NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
        NSLog(@"error %@", newStr);
    }];
}

-(void)counfirm{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *ses = [userDefaults objectForKey:@"sessionId"];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            ses,@"sessionId",
                            @"name",@"user_name",
                            @"12312312",@"user_phone",
                            @"lord-of-the-rock@mail.ru",@"user_email",
                            @"Днепропетровск",@"location_city",
                            @"Карла Маркса",@"location_street",
                            @"117",@"location_house",
                            @"0",@"location_courpus",
                            @"0",@"location_flat",
                            @"2",@"delivery_type",
                            @"house",@"delivery_subtype",
                            @"1",@"payment_type",
                            @"0",@"bonus",
                            nil];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [manager POST:@"http://dev.online.detishop.ru/app/cart/confirm" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"cart %@", responseObject);
        if ([ses length]) {
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:nil forKey:@"sessionId"];
            [userDefaults synchronize];
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Корзина"
                                                                 message:@"Запрос добавлен в оработку. С Вами свяжутся для уточнения деталей покупки"
                                                                delegate:nil
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
            [errorAlert show];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSData *data = operation.responseData;
        NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
        NSLog(@"error %@", newStr);
    }];
}

#pragma mark - Вход, информация и настройки пользователя

-(void) loginUserWithEmail:(NSString*)email andPassword:(NSString*)password complection:(ServerAPICompletion)complection;
{
    NSDictionary *params = [NSDictionary dictionary];
    params = @{email:@"login",password:@"password"};
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager.requestSerializer setValue:@"text/html" forHTTPHeaderField:@"Content-type"];
    [manager  POST:@"http://dev.online.detishop.ru/app/auth" parameters: params
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               NSLog(@"cart %@", responseObject);
               UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Вход"
                                                                    message:responseObject[@"error"][@"messaage"]
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
               [errorAlert show];
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               NSLog(@"Error: %@", error);
               NSData *data = operation.responseData;
               NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
               NSLog(@"error %@", newStr);
           }];
}

-(void) signUpWithEmail:(NSString*)email phone:(NSString*)phone name:(NSString*)name city:(NSString*)city card:(NSString*)card completion:(ServerAPICompletion)completion{

    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager.requestSerializer setValue:@"text/html" forHTTPHeaderField:@"Content-type"];
    [manager  POST:@"http://dev.online.detishop.ru/app/registration" parameters:@{email:@"login",phone:@"phone",name:@"name",card:@"card",city:@"city"}
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"cart %@", responseObject);
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Корзина"
                                                             message:responseObject[@"error"][@"messaage"]
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
        [errorAlert show];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSData *data = operation.responseData;
        NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
        NSLog(@"error %@", newStr);
    }];
}
-(void) getUserSettingWithUserId:(NSString*)userId completion:(ServerAPICompletion)completion{
    
}

-(void) saveUserSettingsWithEmail:(NSString*)email oldPass:(NSString*)oldPass newPass:(NSString*)newPass phone:(NSString*)phone name:(NSString*)name birthDate:(NSString*)birthDate city:(NSString*)city kidName:(NSString*)kidName kidDate:(NSString*)kidDate completion:(ServerAPICompletion)completion{
    
}
@end
