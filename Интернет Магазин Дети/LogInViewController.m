//
//  LogInViewController.m
//  KidShop
//
//  Created by Evgen Sichkar on 1/30/14.
//  Copyright (c) 2014 Evgen Sichkar. All rights reserved.
//

#import "LogInViewController.h"

@interface LogInViewController ()

@end

@implementation LogInViewController{
    ServerAPI *request;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    request = [ServerAPI new];

	// Do any additional setup after loading the view.
}

- (IBAction)login:(id)sender {
    [request loginUserWithEmail:_email.text andPassword:_password.text complection:^(BOOL success, id result, NSError *error) {
        
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
