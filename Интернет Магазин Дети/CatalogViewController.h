//
//  CatalogViewController.h
//  Интернет Магазин Дети
//
//  Created by Evgen Sichkar on 12/25/13.
//  Copyright (c) 2013 Evgen Sichkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerAPI.h"
#import "CartViewController.h"
@interface CatalogViewController : UITableViewController<ServerAPI>
-(void)loadCatalog:(NSMutableArray*)catalog;
@end
