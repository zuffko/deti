//
//  SubCatalogViewController.h
//  Интернет Магазин Дети
//
//  Created by Evgen Sichkar on 12/25/13.
//  Copyright (c) 2013 Evgen Sichkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerAPI.h"
#import "CartViewController.h"
@interface SubCatalogViewController : UITableViewController<ServerAPI>
@property (nonatomic, retain) NSString*catalogId;
@end
