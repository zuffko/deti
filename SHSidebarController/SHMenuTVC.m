//
//  MenuTVC.m
//  TVS
//
//  Created by Jorge Izquierdo on 6/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SHMenuTVC.h"
#import <QuartzCore/QuartzCore.h>


@implementation SHMenuTVC
@synthesize tableView;
-(id)initWithTitlesArray:(NSArray *)array andDelegate:(id<SHMenuDelegate>)del{
    
    self = [super init];
    
    if (self){
        
        delegate = del;
        titlesArray = array;
    }
    return self;
}
- (void)viewDidLoad
{
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,35, 300, self.view.frame.size.height) style:UITableViewStylePlain];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 75)];
    view.backgroundColor=[UIColor colorWithRed:(242/255.0) green:(242/255.0) blue:(242/255.0) alpha:1];
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 31, 285, 44)];
    searchBar.userInteractionEnabled = NO;
    [view addSubview:searchBar];
    [self.view addSubview:view];
    
    UIImageView *shadow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sidebarshadow"]];
    [shadow setFrame:CGRectMake(245, 0, 43.5, self.view.frame.size.height)];
    shadow.autoresizingMask = UIViewAutoresizingFlexibleHeight;

    [self.view addSubview:shadow];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    UIImageView *sbg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sidebarbg"]];
    [sbg setFrame:CGRectMake(0, 0, 300, self.view.frame.size.height)];
    sbg.autoresizingMask = UIViewAutoresizingFlexibleHeight;

    [self.tableView setBackgroundView:sbg];
    self.view.backgroundColor = [UIColor clearColor];
    [super viewDidLoad];

    
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(70, 465, 57, 34)];
    UIImageView *image2 = [[UIImageView alloc] initWithFrame:CGRectMake(145, 470, 67, 22)];
    image.image = [UIImage imageNamed:@"card@2x.png"];
    image2.image = [UIImage imageNamed:@"visa@2x.png"];
    [self.tableView addSubview:image];
    [self.tableView addSubview:image2];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [titlesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (indexPath.row==0) {
        return cell;
    }else{
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(35, 10, 300, 15)];
        label.font = [UIFont fontWithName:@"HelveticaNeue" size:15.0];
        label.text = [titlesArray objectAtIndex:indexPath.row];
        [cell.contentView addSubview:label];
        
        UIImageView *bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sidebarcell"]];
        [bg setFrame:CGRectMake(0, 0, 300, 42.5)];

        cell.backgroundView = bg;
        cell.backgroundColor = [UIColor clearColor];
        UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(5, 9, 25, 19)];
        NSMutableArray *array = [NSMutableArray arrayWithObjects:
                                 [UIImage imageNamed:@"catalog@2x.png"],
                                 [UIImage imageNamed:@"catalog@2x.png"],
                                 [UIImage imageNamed:@"star@2x.png"],
                                 [UIImage imageNamed:@"info@2x.png"],
                                 [UIImage imageNamed:@"car@2x.png"],
                                 [UIImage imageNamed:@"backs@2x.png"],
                                 [UIImage imageNamed:@"pin@2x.png"],
                                 [UIImage imageNamed:@"person@2x.png"],
                                 [UIImage imageNamed:@"news@2x.png"],
                                 [UIImage imageNamed:@"book@2x.png"],
                                 [UIImage imageNamed:@"shop@2x.png"],
                                 [UIImage imageNamed:@"car_time@2x.png"],nil];
        image.image = [array objectAtIndex:indexPath.row];
        [cell.contentView addSubview:image];
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 38;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([delegate respondsToSelector:@selector(didSelectElementAtIndex:)]){
        
        [delegate didSelectElementAtIndex:indexPath.row];
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

@end
